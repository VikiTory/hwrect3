export function getUrl (articul, arr) {
    return articul && arr.find((product) => {
        return product.articul === articul
       })?.url
}