import {
    Router,
    createBrowserRouter,
   } from "react-router-dom";
import Layout from "../Pages/Layout/Layout";
import Favorite from "../Pages/Favorite/Favorite";
import Basket from "../Pages/Basket/Basket";
import Home from "../Pages/Home/Home";



const router = createBrowserRouter([
    {
      path: "/",
      element: <Layout/>, children:[{index: true, element: <Home/>}, {path:"favorite", element:<Favorite/>}, {path:"basket", element:<Basket/>}]
      
    },
    
  ]);
  export default router