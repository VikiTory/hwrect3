import { useOutletContext } from "react-router-dom";
import ProductList from "../../components/ProductList/ProductList";


const Favorite =() => {
    const {favorite, products, togalFavorite, setArtikul} =useOutletContext ();
    const favoriteProducts =
        products.filter((item)=> {
            if(favorite.includes(item.articul)){
                return true 
            }else{
                return false 
            }

        })
    
    return (
        <>  <ProductList products={favoriteProducts} togalFavorite={togalFavorite} favorite={favorite} setArtikul={setArtikul}/>
        </>
    )
    

}
export default Favorite