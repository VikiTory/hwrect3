import ProductList from '../../components/ProductList/ProductList';
import { useOutletContext } from 'react-router-dom';

const Home =() => {
    const {favorite, products, togalFavorite, setArtikul} = useOutletContext();
    
    return (<>  <ProductList products={products} togalFavorite={togalFavorite} favorite={favorite} setArtikul={setArtikul}/>
        </>
)
}       
export default Home