import ModalImage from '../../components/ModalImag';
import ProductList from '../../components/ProductList/ProductList';
import { useOutletContext } from 'react-router-dom';
import TextElement from '../../components/TextElement/TextElement';
import { getUrl } from '../../helpers/getUrl';

const Basket =() => {
    const {favorite, basket, products, togalFavorite, setArtikul, currentArtikul, deleteFromBasket, closeModal} =useOutletContext ();
    const basketProducts =
        products.filter((item)=> {
            if(basket.includes(item.articul)){
                return true 
            }else{
                return false 
            }

        })
        const currentImg = getUrl(currentArtikul, basketProducts)
    return (
        <>  <ProductList products={basketProducts} togalFavorite={togalFavorite} favorite={favorite} setArtikul={setArtikul}/>
        {currentArtikul && (
          <ModalImage
          
          closeButton={true}
          text={<>
          <TextElement type='h2' marginB='32px'>Product Delete?!</TextElement>
          <TextElement marginB='64px'>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</TextElement>
          </>}
          closeModal={closeModal}
          footerProps={{firstText:"NO, CANCEL", secondaryText:"YES, Delete from Basket", firstClick:closeModal, secondaryClick:()=> {deleteFromBasket(currentArtikul);closeModal()}, firstClass:'cancel', firstBackground:'#8A33FD'}}
          imageSrs={currentImg}
         
         />
        )}
        </>
    )
    
    

}
export default Basket