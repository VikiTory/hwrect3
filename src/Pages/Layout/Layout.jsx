import { useEffect, useState } from 'react';
import './App.css';
import Header from '../../components/header/Header';
import { Outlet, useLocation } from 'react-router-dom';
import ModalImage from '../../components/ModalImag';
import ModalText from '../../components/ModalText';
import TextElement from '../../components/TextElement/TextElement';
import { getUrl } from '../../helpers/getUrl';

const Layout = () => {
    const initialFavorite = localStorage.getItem("fav")?JSON.parse(localStorage.getItem("fav")):[];
    const initialBasket = localStorage.getItem("basket")?JSON.parse(localStorage.getItem("basket")):[];
    const [products, setProducts] =useState([]);
    const [favorite, setFavorite] =useState(initialFavorite);
    const [basket, setBasket] =useState(initialBasket);
    const [currentArtikul, setCurrentArtikul ] = useState(null);
    let location = useLocation(); console.log(location)
    const isLocationBasket = location.pathname === "/basket"
    const isBasket = basket?.includes(currentArtikul); 
    const addToBasket = (articul) => {
      setBasket((prev) => { 
        const isBasket = prev.includes(articul) 
        if (!isBasket) {
          return [...prev,articul]
        } else {
          return prev
        }
      })
  
    }
    const deleteFromBasket = (articul) => {
        setBasket((prev) => { console.log(prev, articul)
        return prev.filter((item) => {
            if(item !== articul){
                return true 
            }else {return false}

        })
        })
    }
    const togalFavorite = (articul) => {
      setFavorite((prev) => {console.log(prev)
        const isFavorite = prev.includes(articul)
      if (isFavorite) { return prev.filter((favId) => {
       if (favId===articul) {
       return false
       } else {
        return true 
       }
      })
      }else { 
       return [...prev,articul]
      }
      })
      
    }
  
    // const [ showModal2, setShowModal2 ] = useState(false);
  
    const setArtikul = (artikul) => {
      setCurrentArtikul(artikul);
    };
  
    // const openModal2 = () => {
    //   setShowModal2(true);
    // };
  
    const closeModal = () => {
      setCurrentArtikul(null);
    };
  
    // const closeModal2 = () => {
    //   setShowModal2(false);
    // };
    useEffect(() => {
      fetch("./product.json")
      .then((response) => 
       response.json()
      ).then((data) => {
        setProducts(data)
      })
    }, []);
    useEffect(()=> {
      localStorage.setItem("fav", JSON.stringify(favorite))
      localStorage.setItem("basket", JSON.stringify(basket))
  
    },[favorite,basket])
    
    const currentImg = getUrl(currentArtikul, products)

    return (
      
      
      <div className="App">
        <Header favorite={favorite} basket={basket}/>
        <Outlet context={{favorite, basket, products, togalFavorite, setArtikul, currentArtikul, isBasket, closeModal, addToBasket, deleteFromBasket}}/>
        {currentArtikul &&!isBasket&& (
          <ModalImage
          
          closeButton={true}
          text={<>
          <TextElement type='h2' marginB='32px'>Product Add!</TextElement>
          <TextElement marginB='64px'>By clicking the “Yes, Add” button, PRODUCT NAME will be added.</TextElement>
          </>}
          closeModal={closeModal}
          footerProps={{firstText:"NO, CANCEL", secondaryText:"YES, Add To Basket", firstClick:closeModal, secondaryClick:()=> {console.log(currentArtikul);addToBasket(currentArtikul);closeModal()}, firstClass:'cancel', firstBackground:'#8A33FD'}} 
          imageSrs={currentImg}
         
         />
        )}
        
        {isBasket &&!isLocationBasket&& (
     <ModalText
    
    closeButton={true}
    text={<>
       
     <TextElement marginB='73px'>Product Added To Basket</TextElement>
     </>}
     closeModal={closeModal}
     footerProps={{firstText:"OK, CLOSE", firstClick:closeModal, firstBackground:'#8A33FD', firstClass:'cancelWithMargin'}}
    />
   )}
  
  
  
      </div>
    );
  };
  
  
 
  export default Layout

