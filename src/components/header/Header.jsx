import {ReactComponent as FavoriteIcon} from "../../assets/favorite.svg"
import {ReactComponent as BasketIcon} from "../../assets/basket.svg"
import PropTypes from 'prop-types';
import styles from "./Header.module.scss"
import { Link } from "react-router-dom";
const Header = ({favorite, basket})=>{
    return (
        <header className={styles.header}><div className={`container`}><nav>
        <ul className={styles.wrapper}>
          <li>
            <Link className={styles.link} to="/">Home</Link>
          </li>
          <li>
            <Link className={styles.link} to="/favorite"><FavoriteIcon/>{favorite.length}</Link>
          </li>
          <li>
            <Link className={styles.link} to="/basket"><BasketIcon/>{basket.length}</Link>
          </li>
        </ul>
      </nav></div></header>
    )
}
Header.propTypes = {
    favorite: PropTypes.array,
    basket: PropTypes.array,
    
  };
export default Header